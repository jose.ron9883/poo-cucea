/* 
   Programación Orientada a Objetos
   Actividad 02: Tipos de datos
   by: JMRZ
*/

import java.util.Scanner; 

public class Actividad {

  public static void Actividad(String[] args) {
    
    Scanner scanner = new Scanner(System.in);
    
    System.out.print("Ingrese el primer número: ");
    int num1 = scanner.nextInt();

    System.out.print("Ingrese el segundo número: ");
    int num2 = scanner.nextInt();

    int suma = num1 + num2;
    
    System.out.println("La suma es: " + suma);

  }

}