// Así se pone un comentario en Java, una sola linea

/*
   Así se crea un comentario en Java, multilinea 
*/

/* 
   Programación Orientada a Objetos
   Actividad 02: Tipos de datos
   by: JMRZ
*/

public class Actividad02 {
     public static void main(String arg[]){
     		// Variables
		byte edad = 12; 
		System.out.println(edad); // Imprimir en pantalla la variable edad
	   
		// Floats
		float peso = 75.5f; 	     // Se debe de añadir f (Puede ser mayusculas o minusculas), para que Java identifique que es un flotante. 
		double estatura = 1.75;      // se puede utilizar double, aunque se desperdiciará mucha memoria, tambien se le puede añadir la d.
	
		System.out.println(peso); 
		System.out.println(estatura);

		// Boolean
		boolean casado = true; 
		System.out.println(casado); 

		// Char
     		char primerLetraNombre ='J';  // Cuando es un solo caracter java si hace distinción obligatorio comilla simple
		System.out.println(primerLetraNombre); 

		//String (No primitivo)
		String nombre = "Jose Maria Ron Zamora" ; 
		System.out.println(nombre); 
	
		// Constante 
		final float PI = 3.14f;  // final es la palabra que nos identifica que es una constante.
		System.out.println("Yo soy PI y valgo: " + PI); 
		
		//Arrays
		int[] edades = {14,20,30,50};
		String nombres[] = {"Chema", "Antonio", "Pedro", "Juan"};

		System.out.println(edades[0]); 
		System.out.println(nombres[2]); 


     }	
}

