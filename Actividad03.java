/* 
   Programación Orientada a Objetos
   Actividad 03: Uso de variables
   by: JMRZ
*/

public class Actividad03 {
     public static void main(String arg[]){
	
	//Suma
	float num1 = 150f, num2 = 7f, resultado; // Declaro como flotante por posibles usos en el futuro.
	resultado = (num1 + num2);
	System.out.println("El resultado de sumar " + num1 + " + " + num2 + " es: " + resultado); 

	// Resta
	resultado = (num1 - num2);
	System.out.println("El resultado de restar " + num1 + " - " + num2 + " es: " + resultado); 

	// Multiplicar
	resultado = (num1 * num2);
	System.out.println("El resultado de multiplicar " + num1 + " * " + num2 + " es: " + resultado); 

	// Dividir
	resultado = (num1 / num2);
	System.out.println("El resultado de dividir " + num1 + " / " + num2 + " es: " + resultado); 

	// Dividir con modulo
	resultado = (num1 % num2);
	System.out.println("El residuo de dividir " + num1 + " % " + num2 + " es: " + resultado); 

	// Jerarquia
	resultado = ((num1 + num2) * (5 / 9));
	System.out.println("El el resultado de la expresion es " + resultado); 

	// Jerarquia
	System.out.println("Potencia " + Math.pow(num1,num2)); 

     }	
}
