/* 
   Programación Orientada a Objetos
   Actividad 07: Operadores aritméticos
   by: JMRZ
*/

import java.util.Scanner; 

public class Actividad07 {
     public static void main(String arg[]){


	Scanner scanner = new Scanner(System.in);
	float mate = 0, esp = 0, hist = 0, ing = 0, promedio = 0; 
	

	int condicional = 0; 

	while ( condicional == 0 ){
    		System.out.print("Ingrese la calificación de mate: ");
    		mate = scanner.nextInt();

    		System.out.print("Ingrese la calificación de español: ");
    		esp = scanner.nextInt();

    		System.out.print("Ingrese la calificación de historia: ");
    		hist = scanner.nextInt();

    		System.out.print("Ingrese la calificación de ingles: ");
    		ing = scanner.nextInt();

		promedio = (mate + esp + hist + ing) / 4;
	
		System.out.println("Juanito saco en mate: " + mate + " en Español: " + esp + " en historia, " + hist + " y en ingles: " + ing); 
		System.out.println("Por lo que el promedio de juanito de esas 4 materias fue de: " + promedio); 

		if(promedio >= 6){
			System.out.println("Juanito aprobó con: " + promedio); 	
		}else{

			System.out.println("Juanito no aprobó, =( , reprobó con: " + promedio); 
		}

		System.out.print("¿Quieres volver a realizar el ejercicio? 0.- Si , 1.- No");
		condicional = scanner.nextInt();

     }	
   }	
}
