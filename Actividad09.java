/* 
   Programación Orientada a Objetos
   Actividad 09: Estructura condicional
   by: JMRZ
*/

public class Actividad09 {
    public static void main(String[] args) {
        
        int num1 = 100, num2 = 500, num3 = 100 ;
        
        System.out.println("Los numeros insertados son: " + num1 + " - " + num2 + " - " + num3);
        System.out.println("Los numeros ordenados quedan así: ");


        if(num1 > num2 && num1 > num3 ){
            System.out.println(num1);
            
            if(num2 > num3){
                System.out.println(num2);
                System.out.println(num3);
            }else{
                System.out.println(num3);
                System.out.println(num2);
            }
        }else if(num2 > num3 && num2 > num1){
            System.out.println(num2);

            if(num1 > num3){
                System.out.println(num1);
                System.out.println(num3);
            }else{
                System.out.println(num3);
                System.out.println(num1);
            }

        }else if(num3 > num1 && num3 > num2){
            System.out.println(num3);

            if(num2 > num1){
                System.out.println(num2);
                System.out.println(num1);
            }else{
                System.out.println(num2);
                System.out.println(num1);
            }
        }
    }
}