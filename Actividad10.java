/* 
   Programación Orientada a Objetos
   Actividad 10: Factorial
   by: JMRZ
*/

import java.util.Scanner; 

public class Actividad10 {
    public static void main(String[] args) {
        Scanner output = new Scanner(System.in);

        int numFact = 0, ciclo = 1; 
        
        System.out.println("Este programa generá el factorial del numero ingresado por el usuario");

        do{

            long facto = 1; 

            System.out.println("Ingresa el numero del cual quieras conocer su factorial");
            numFact = output.nextInt();

            for(int i = 1 ; i <= numFact ; i++){

                facto = facto * i; 
            }

            System.out.println("El factorial de: " + numFact + " es: " + facto);


            do{

                System.out.println("¿Quieres calcular el factorial de otro numero? 1.- Si, 0.- No" );
                ciclo = output.nextInt();
    
                if ( ciclo != 0 && ciclo  != 1 ){
                    System.out.println("Opcion no valida, ingresa 1 o 0 por favor");
                }
        
            }while(ciclo != 0 && ciclo  != 1);


        }while(ciclo == 1);

        System.out.println("Se finalizó el programa, gracias.");
 
    }
}