/* 
   Programación Orientada a Objetos
   Actividad: Actividad 11
   by: JMRZ
*/


package cucea.poo.chema.poo.Actividad11;
import cucea.poo.chema.poo.Poo;
import java.util.Scanner;



public class Actividad11 {
    String nombre; 
    int edad;
    
    // Constructor para instanciar las variables 
    public Actividad11(String nombre, int edad) {
        this.nombre = nombre; 
        this.edad = edad; 

    }
    
    // Primer metodo
    public void mayorEdad(){
        
        if(edad >= 18){
            System.out.println("Tu nombre es " + nombre + " y tu edad es: " + edad + " eres mayor de edad");
        }else{
            System.out.println("Eres menor de edad " + nombre + " tienes " + edad + " años");
        }
    }
    
    public static void main(String[] args) {
        
        Scanner output = new Scanner(System.in);
        String name;
        int age;
        
        // Entrada de los datos
        System.out.println("Ingresa por favor los datos solicitados");
        System.out.println("Ingresa por favor tu nombre");
        name = output.nextLine();
        
        System.out.println("Ingresa por favor tu edad");
        age = output.nextInt();
        
        // Creación del objeto
        Actividad11 persona01 = new Actividad11(name,age);
        // llamada a metodo verificador de edad
        persona01.mayorEdad();
        
    }  
}
