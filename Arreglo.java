
package cucea.poo.chema.arreglos;


public class Arreglo {
    public static void main(String[] args) {
        
        int array[] = new int[4]; 
        String familia [] = new String [4];
        
        array[0] = 20;
        array[1] = 15;
        array[2] = 10;
        array[3] = 55;
        
        familia [0] = "Chema";
        familia [1] = "Jose Maria";
        familia [2] = "Maria Jose";
        familia [3] = "Joseph Marie";
        
    
        for (int i = 0 ; i < array.length ; i++ ){
            System.out.println(array[i]);
        }
        
        // foreach
        for(String tmp:familia){
            System.out.println(tmp);
        }
        
        
    }
}
