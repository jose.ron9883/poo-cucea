/* 
   Programación Orientada a Objetos
   Actividad 08: Condicionales anidados
   by: JMRZ
*/

import java.util.Scanner; 

public class Calculadora {
     public static void main(String args[]){

	
	Scanner scanner = new Scanner(System.in);
	int operacion = 1, ciclo = 0; 
	double num1 , num2, resultado; 

	do{
		System.out.print("Ingrese el primer número: ");
        	num1 = scanner.nextInt();

		System.out.print("Ingrese el segundo número: ");
        	num2 = scanner.nextInt();

	
		System.out.print("¿Que operacion quieres realizar? ");
		System.out.print("1.- Suma ");
		System.out.print("2.- Resta ");
		System.out.print("3.- Multiplicacion ");
		System.out.print("4.- Division ");
        	operacion = scanner.nextInt();

		if (operacion == 1){  // suma de dos numeros
			resultado = num1 + num2; 
			System.out.print("El resultado de la operacion es: " + resultado);
		
		}else if(operacion == 2){ // resta de dos numeros
			resultado = (num1 - num2); 
			System.out.print("El resultado de la operacion es: " + resultado);
		
		}else if(operacion == 3){ // multiplicacion de dos numeros
			resultado = num1 * num2; 
			System.out.print("El resultado de la operacion es: " + resultado);	
	
		}else if(operacion == 4){ // division de dos numeros
			resultado = num1 / num2; 
			System.out.print("El resultado de la operacion es: " + resultado);		
		}

		
		System.out.print("¿Quieres realizar una nueva operacion? 1-Si, 2.- No ");
		ciclo = scanner.nextInt();

	}while(ciclo == 1); 
	
   }	
}
