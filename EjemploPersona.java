
package cucea.poo.chema.encapsulamiento;

class Persona{
    String nombre ; 
    private int edad; 
    boolean genero;
    private double calif; 
    
    public Persona(){
    
    }
    
    //Metodo controlado para realizar el encansulamiento
    public void setEdad(int edad){
        if((edad > 0) && (edad < 150)){
            this.edad = edad;
        }else{
            System.out.print("No es una edad valida!, ingresaste -> ");
        }
    }
    
    public int getEdad(){
        return edad; 
    }
    
    public void setCalif(double calif){
        if ( calif > 0 && calif <= 100){
            this.calif = calif; 
        }else{
            System.out.print("Ingresa una calificación valida entre 0 - 100, ingresaste -> ");
        }
    }
    
    public double getCalif(){
        return calif; 
    }
    
}

public class EjemploPersona {
    public static void main(String[] args) {
        
        Persona pepe = new Persona(); 
        pepe.setEdad(156);
        System.out.println(pepe.getEdad());
        
        pepe.setCalif(98);
        System.out.println(pepe.getCalif());
        
    }
}
