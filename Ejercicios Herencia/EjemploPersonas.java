
package cuce.poo.chema.poo.herencia;

public class EjemploPersonas {
    public static void main(String[] args) {
        Estudiante juanito = new Estudiante(); 
        juanito.nombre = "Juan Eduardo";
        juanito.fechaNacimiento = "15/11/1998";
        juanito.dormir();
        juanito.respirar();
    }
    
}
