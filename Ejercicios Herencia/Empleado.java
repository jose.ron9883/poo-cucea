package cuce.poo.chema.poo.herencia;

public class Empleado extends Persona {
    double sueldo; 
    
    public void trabajar(){
        System.out.println("Mi nombre es: " + nombre + " y estoy trabajando ");
    }
    
    public void cobrar(){
        System.out.println("Mi nombre es: " + nombre + " y cobro " + sueldo);
    }
}
