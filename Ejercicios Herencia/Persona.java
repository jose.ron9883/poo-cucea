/* 
   Programación Orientada a Objetos
   Actividad: Persona con herencia
   by: JMRZ
*/
package cuce.poo.chema.poo.herencia;


public class Persona {
    String nombre; 
    String fechaNacimiento; 
    
    public void dormir(){
        System.out.print("Mi nombre es: " + nombre + " y estoy dormido");
    }
    
    public void respirar(){
        System.out.print("Mi nombre es: " + nombre + " y estoy respirando desde " + fechaNacimiento);
    }
    
    public void comer(){
        System.out.print("Mi nombre es: " + nombre + " y estoy comiendo ");
    }
    
}
