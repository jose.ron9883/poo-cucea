/* 
   Programación Orientada a Objetos
   Actividad 09: Estructura condicional Switch
   by: JMRZ
*/

import java.util.Scanner; 

public class EstructuraSwitch {
    public static void main(String[] args) {

        Scanner output = new Scanner(System.in);
        String nombre;

        System.out.println("Introduce tu nombre: ");
        nombre = output.nextLine();

        System.out.println("Tu nombre es: " + nombre);


    }
}