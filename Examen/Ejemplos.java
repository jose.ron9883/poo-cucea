package cucea.poo.chema.ejemplo;
import java.util.Scanner;
 
class Examen{
    private String saludo ,nombre; 
    private double calificacion;
    
    public Examen(){
        
    }

    public double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion() {
        Scanner in = new Scanner(System.in); 
        
        do{
            System.out.println("Ingresa la calificacion del estudiante");
            calificacion = in.nextInt();


            if(calificacion >= 0 && calificacion <= 100){
                this.calificacion = calificacion;    
            }else{
                System.out.println("El valor ingresado no es valida, valida solo ( 0 - 100) ");
            }

        }while(calificacion < 0 || calificacion > 100);
       
    }

}

public class Ejemplos { 

    public Ejemplos() {
    }
    
    
    public static void main(String[] args) {

        
        Examen objeto01 = new Examen();
        objeto01.setCalificacion();
        
    }
}