/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cucea.poo.chema.ejemplo;

abstract class medioTransporte{
    private String marca; 
    
   abstract void avanzar();
}

class Carro extends medioTransporte{
    
    @Override
    void avanzar(){
        System.out.println("Voy avanzando por carretera");
    }
}

class Avion extends medioTransporte{
    @Override
    void avanzar(){
        System.out.println("Voy avanzando por Aire");
    } 
}

class Barco extends medioTransporte{
    @Override
    void avanzar(){
        System.out.println("Voy avanzando por los pasillos de CUCEA");
    }  
}

public class Ejemplos01 {
    
    public void crearTransporte(medioTransporte transporte){
        transporte.avanzar();
    }
    
    public static void main(String[] args) {
        Ejemplos01 e = new Ejemplos01();
        e.crearTransporte(new Avion());
    }
}
