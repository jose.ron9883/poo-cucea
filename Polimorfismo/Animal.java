/*
   Programación Orientada a Objetos
   Actividad 13 : Encapsulamiento
   by: JMRZ
*/
package cucea.poo.chema.polimorfismo;

public class Animal {
    private double peso;
    private int edad; 
    private String genero; 
    
    public Animal(double peso, int edad, String genero){
        this.peso = peso;
        this.edad = edad; 
        this.genero = genero; 
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public String hacerSonido(){
        return "Estoy haciendo un sonido";
    }
    
    public void comer(){
        System.out.println("Estoy comiendo");
    }
    
    public void desplazarse(){
        System.out.println("Me estoy desplazando ");
    }
    
    
}
