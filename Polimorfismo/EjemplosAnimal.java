/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cucea.poo.chema.polimorfismo;

public class EjemplosAnimal {
    
    public void crearAnimal(Animal animal){
        
        System.out.println("Peso: " + animal.getPeso() + " kg, tengo " + animal.getEdad() + " años, soy " 
                           + animal.getGenero() + " y hago " + animal.hacerSonido());
        
    }
    
    public static void main(String[] args) {
        /*
        Animal animal01 = new Animal(2.4,4,"hembra");
        animal01.hacerSonido();
        System.out.println(animal01.hacerSonido());
        
        Animal perro01 = new Perro("Guau Guau Guau", 2.4, 4, "Macho");
        System.out.println(perro01.hacerSonido());
        */
        
        
        // 
        EjemplosAnimal Animal1 = new EjemplosAnimal(); 
        Animal1.crearAnimal(new Perro("Guau Guau",2.5,5,"Machote"));
    
      
    }
}
