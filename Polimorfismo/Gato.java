package cucea.poo.chema.polimorfismo;

public class Gato extends Animal {
    private String sonido; 

    public Gato(String sonido, double peso, int edad, String genero) {
        super(peso, edad, genero);
        this.sonido = sonido;
    }
    

    @Override // Que de prioridad sobre el padre
    public String hacerSonido(){
        return sonido; 
    }
    
}
