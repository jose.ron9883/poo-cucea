/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cucea.poo.chema.polimorfismo;

public class Perro extends Animal {
    private String sonido; 

    public Perro(String sonido, double peso, int edad, String genero) {
        super(peso, edad, genero);
        this.sonido = sonido;
    }
    
    @Override // Sobre escritura, como es heredada, esto permite que esto se muestre 
    public String hacerSonido(){
        return sonido; 
    }
    
}
