
/* 
   Programación Orientada a Objetos
   Actividad: Constructores
   by: JMRZ
*/

package cucea.poo.chema.poo;
import java.util.Scanner;


public class Poo {
    
    String nombre; 
    int edad; 
    
    // Constructor Vacio
    public Poo(){
    
    }
    
    // Constructor
    public Poo(String nombre, int edad) {

        this.nombre = nombre; 
        this.edad = edad; 

    }
    
    // Constructor 
    public Poo(String nacionalidad){
        System.out.println("Yo soy de " + nacionalidad);
    }
  

    // Primer metodo
    public void mayorEdad(){
        if(edad >= 18){
            System.out.println("Tu nombre es " + nombre + " y tu edad es: " + edad);
        }else{
            System.out.println("Eres menor de edad " + nombre + " tienes " + edad + " años");
        }
    }
    
    
    public static void main(String[] args) {
    
       // Creación de Objeto
       Poo people01 = new Poo("Chema", 20); 
       people01.mayorEdad();
       
       // Creación de Objeto 2
       Poo people02 = new Poo("Eduardo" , 17); 
       people02.mayorEdad(); 
    
    }
}
