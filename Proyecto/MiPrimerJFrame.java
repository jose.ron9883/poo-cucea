
package cucea.poo.chema.swing;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class MiPrimerJFrame {
    
    public MiPrimerJFrame(){
        //JFRAME
        JFrame frame = new JFrame();
        frame.setTitle("Mi Primer JFrame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        frame.setSize(400, 400);
        
        // JPanel
        JPanel panel = new JPanel();
        
        // JLabel
        JLabel etiqueta = new JLabel("Ingresa algo"); 
        int fontSize = 14; 
        Font myFont = new Font("Helvetica",Font.ITALIC, fontSize);
        etiqueta.setFont(myFont);
        
        // JTextField 
        JTextField campoTexto = new JTextField(10); 
        campoTexto.setBorder(new LineBorder(Color.BLACK, 1));
        
        // JButton
        JButton btn = new JButton("Soy boton"); 
        
        // Añadir al Frame
        frame.getContentPane().add(panel);
        panel.add(etiqueta); 
        panel.add(campoTexto);
        panel.add(btn);
        
        
        frame.setVisible(true);
    }
    

    
    
    
    public static void main(String[] args) {
        
        new MiPrimerJFrame(); 
        
    }
    
    
}
