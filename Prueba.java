/* 
   Programación Orientada a Objetos
   Actividad 07: Operadores aritméticos
   by: JMRZ
*/

import java.util.Scanner; 

public class prueba {
     public static void main(String args[]){

	int edadMinima = 18;
	float califMin = 8f;
	Scanner scanner = new Scanner(System.in);


    	System.out.print("Ingresa tu edad: ");
    	edadMinima = scanner.nextInt();

	
	if( edadMinima >= 18 ){
		System.out.println(" Pasaste el primer filtro ");

    		System.out.print("Ingrese la calificacion del examen: ");
    		califMin = scanner.nextInt();

		if( califMin >= 8 ){
			System.out.println(" Felicidades! Pasaste el examen, ya puedes recoger tu licencia ");
		}else{
			System.out.println(" Lo siento, no pasaste el examen, vuelve a intentarlo");	
		}

	}else{
		System.out.println(" No cuentas con la edad minima para tenner licencia");	
	}
	
   }	
}

