/*
   Programación Orientada a Objetos
   Actividad 13 : Encapsulamiento
   by: JMRZ

*/
package CUCEA.chema.ejercicios;

class Persona{
    private String nombre, direccion;
    private int edad;
    
    // Constructor
    public Persona(){
    
    }

    // -------------------------Método controlado para realizar los encapsulamientos--------------------------
    
    // Set and Get de Nombre
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getNombre(){
        System.out.println("Nombre: " + nombre);
        return nombre; 
    }

    // Set and Get de Dirección
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        System.out.println("Direccion: " + direccion);
        return direccion;
    }
    
    // Set and Get de Edad
    public void setEdad(int edad){
        if(edad > 0 && edad <=120){
            this.edad = edad;   
        }else{
            System.out.println("Ingresa una edad valida, que esté entre 1 y 120");
        }
    }

    public int getEdad() {
        System.out.println("Edad: " + edad);
        return edad;
    }
    
}


//Inicio de funcion principal
public class PersonaMain {
    public static void main(String[] args) {
        
        //------------------------ Creación de los objetos -----------------------------------
        
        //Objeto 1
        Persona persona01 = new Persona(); 
        persona01.setNombre("Jose Maria Ron");
        persona01.setDireccion("Calle Tesistan #478, La Venta del Astillero, Zapopan, Jalisco.");
        persona01.setEdad(25);
        
        // Objeto 2
        Persona persona02 = new Persona(); 
        persona02.setNombre("Chema Ron Zamora");
        persona02.setDireccion("Periferico Norte No. 799 Nucleo Universitario, C. Prol. Belenes, Zapopan, Jal.");
        persona02.setEdad(25);
        
        // ----------------------- Impresion de los objetos -------------------------------------
        // Objeto 01 (Persona 1)
        System.out.println("Persona 1:");
        persona01.getNombre();
        persona01.getEdad();
        persona01.getDireccion();
        
        // salto de linea unicamente estetico
        System.out.println();
        
        // Objeto 02 (Persona 2)
        System.out.println("Persona 2:");
        persona02.getNombre();
        persona02.getEdad();
        persona02.getDireccion();
    }
    
}// fin función principal
