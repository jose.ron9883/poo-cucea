/* 
   Programación Orientada a Objetos
   Actividad: Examen parcial 1
   by: JMRZ

*/

import java.util.Scanner;

public class examen {
    public static void main(String[] args) {
        
        Scanner output = new Scanner(System.in);
        int longitud = 0;
        String nombre; 
    
            do{

                System.out.println("Ingresa algun nombre que tu quieras ");
                nombre = output.nextLine();  
                longitud = nombre.length(); 

                if(longitud < 2 || longitud > 20 ){  
                    System.out.println("*** ¡Solo funciona con cadenas de entre 2 y 20 caracteres! ***");
                    break;
                }

                System.out.print("La cadena invertida es: " + " ");
                for(int i  = longitud - 1; i >= 0 ; i--){
                    System.out.print(nombre.charAt(i));
                }
                System.out.println(" y contiene: " + longitud + " caracteres");
  
            }while(longitud >= 2 && longitud <= 20);   
    }
}